export const applicationJsonHeadersContentType = {
  header: {
    "Content-Type": "application/json",
  },
};

export const multipartFormHeadersContentType = {
  header: {
    "Content-Type": "multipart/form-data",
  },
};
