import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./components/Page/HomePage";
import { LoginPage } from "./components/Page/LoginPage";
import { RegisterPage } from "./components/Page/RegisterPage";
import ProductDetailPage from "./components/Page/ProductDetailPage";
import ProductCollectionsPage from "./components/Page/ProductCollectionsPage";
import CartSumary from "./components/Page/CartSummaryPage";
import CheckoutPage from "./components/Page/CheckoutPage";
import AccountPage from "./components/Page/AccountPage";
import AdminPage from "./components/Page/AdminPage";
import ProductManagement from "./components/Admin/ProductManagement";
import CreateProductPage from "./components/Admin/CreateProductPage";
import { CssBaseline } from "@mui/material";
// import HomePage from "./components/Page/homepage.page";
import RequireUser from "./components/requireUser";
// import ProfilePage from "./components/Page/profile.page";
// import AdminPage from "./components/Page/adminpage.page";
import UnauthorizePage from "./components/Page/unauthorizePage";
import EmailVerificationPage from "./components/Page/VerifyEmail.page";
//import LoginPage from "./components/Page/Login.page";
// import RegisterPage from "./components/Page/Register.page";
import { Toaster } from "react-hot-toast";
import UpdateProductPage from "./components/Admin/UpdateProduct";
import OrderManagement from "./components/Admin/OrderManagement";

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  //<React.StrictMode>
  <Provider store={store}>
    <BrowserRouter>
      <CssBaseline />
      <Toaster position="top-right" />
      <Routes>
        <Route path="/" element={<App />}>
          <Route path="unauthorized" element={<UnauthorizePage />} />

          <Route index element={<HomePage />} />
          <Route path="login" element={<LoginPage />} />
          <Route path="register" element={<RegisterPage />} />
          <Route path="/product/:productid" element={<ProductDetailPage />} />
          <Route path="/products" element={<ProductCollectionsPage />} />
          <Route path="/order_summary" element={<CartSumary />} />
          <Route path="/check_out" element={<CheckoutPage />} />
          <Route path="/my_account" element={<AccountPage />} />

          <Route path="verifyemail" element={<EmailVerificationPage />}>
            <Route
              path=":verificationCode"
              element={<EmailVerificationPage />}
            />
          </Route>
        </Route>
        <Route element={<RequireUser allowedRoles={["admin"]} />}>
          <Route path="/admin" element={<AdminPage />}>
            <Route path="product_management" element={<ProductManagement />} />
            <Route path="create_product" element={<CreateProductPage />} />
            <Route path="update_product/:id" element={<UpdateProductPage />} />
            <Route path="order_management" element={<OrderManagement />} />
          </Route>
        </Route>
      </Routes>
  
    </BrowserRouter>
  </Provider>
  // </React.StrictMode>
);
