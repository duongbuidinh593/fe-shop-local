import { createSlice } from "@reduxjs/toolkit";
import { fetchAllCart } from "./../cart/cartActions";
import { createOrder, fetchAllOrder } from "./orderActions";

const initialState = {
  status: "idle",
  orderItems: [],
};

export const orderSlice = createSlice({
  name: "order",
  initialState,
  reducers: {
    resetOrder: (state) => {
      state.orderItems = [];
    },
  },
  extraReducers: {
    //Fetch Order
    [fetchAllOrder.pending]: (state, { payload }) => {
      state.status = "loading";
    },
    [fetchAllOrder.fulfilled]: (state, { payload }) => {
      state.status = "idle";
      state.orderItems = payload;
    },
    [fetchAllOrder.rejected]: (state, { payload }) => {
      console.log(payload);
    },

    //Create order
    [createOrder.pending]: (state) => {
      state.status = "creating";
    },
    [createOrder.fulfilled]: (state, { payload }) => {
      state.status = "idle";
      state.orderItems.push(payload);
    },
    [createOrder.rejected]: (state, { payload }) => {
      console.log(payload);
    },
  },
});

export default orderSlice;

export const { resetOrder } = orderSlice.actions;
