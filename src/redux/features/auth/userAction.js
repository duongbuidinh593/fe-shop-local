// userAction.js
import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

import { useNavigate } from "react-router-dom";
import { fetchAllCart } from "./../cart/cartActions";
import { fetchAllOrder } from "../order/orderActions";

import {
  getMeFn,
  loginUserFn,
  refreshAccessTokenFn,
  signUpUserFn,
  verifyEmailFn,
} from "../../api/authAPI1";


export const userLogin = createAsyncThunk(
  "user/login",
  async ({ email, password }, { rejectWithValue, dispatch }) => {
    try {
      const data = await loginUserFn({ email, password });

      return data;
    } catch (error) {
      // return custom error message from API if any
      if (error.response && error.response.data.message) {
        return rejectWithValue(error.response.data.message);
      } else {
        return rejectWithValue(error.message);
      }
    }
  }
);

export const getUserDetails = createAsyncThunk(
  "user/getUserDetails",
  async (arg, { getState, rejectWithValue, dispatch }) => {
    try {
      const data = await getMeFn();

      return data;
    } catch (error) {
      if (error.response && error.response.data.message) {
        return rejectWithValue(error.response.data.message);
      } else {
        return rejectWithValue(error.message);
      }
    }
  }
);

export const getNewAccessToken = createAsyncThunk(
  "user/getNewAccessToken",
  async (arg, { getState, rejectWithValue, dispatch }) => {
    try {
      const data = await refreshAccessTokenFn();

      dispatch(getUserDetails());
      dispatch(fetchAllCart());

      return data;
    } catch (error) {
      if (error.response && error.response.data.message) {
        return rejectWithValue(error.response.data.message);
      } else {
        return rejectWithValue(error.message);
      }
    }
  }
);

export const registerUser = createAsyncThunk(
  // action type string
  "user/register",
  // callback function
  async ({ name, email, password, passwordConfirm }, { rejectWithValue }) => {
    try {
      // configure header's Content-Type as JSON
      const data = await signUpUserFn({
        name,
        email,
        password,
        passwordConfirm,
      });
      return data;
    } catch (error) {
      // return custom error message from API if any
      if (error.response && error.response.data.message) {
        return rejectWithValue(error.response.data.message);
      } else {
        return rejectWithValue(error.message);
      }
    }
  }
);

export const verifyEmail = createAsyncThunk(
  "email/verify",
  async ({ verificationCode }, { rejectWithValue }) => {
    try {
      const data = await verifyEmailFn(verificationCode);
      return data;
    } catch (error) {
      if (error.response && error.response.data.message) {
        return rejectWithValue(error.response.data.message);
      } else {
        return rejectWithValue(error.message);
      }
    }
  }
);
