import { createSlice } from "@reduxjs/toolkit";
import React from "react";
import {
  addCartItem,
  fetchAllCart,
  removeCartItem,
  updateCart,
  removeAllCartWithUserName,
} from "./cartActions";
const initialState = {
  status: "ilde",
  cartItems: [],
};

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    removeItem: (state, payload) => {
      state.cartItems.splice(
        state.cartItems.findIndex((arrow) => arrow.id === payload.id),
        1
      );
    },
    resetCart: (state) => {
      state.cartItems = [];
    },
  },
  extraReducers: {
    //Add cart
    [addCartItem.fulfilled]: (state, { payload }) => {
      state.cartItems.push(payload);
      state.status = "idle";
    },

    //Fetch data
    [fetchAllCart.pending]: (state) => {
      state.status = "loading";
    },
    [fetchAllCart.fulfilled]: (state, { payload }) => {
      state.status = "idle";

      state.cartItems.push(...payload);
    },
    [fetchAllCart.rejected]: (state, { error }) => {
      console.log(error);
    },

    //Update cart
    [updateCart.fulfilled]: (state, { payload }) => {
      const index = state.cartItems.findIndex((c) => c.id == payload.id);
      state.cartItems[index] = {
        ...state.cartItems[index],
        quantity: payload.quantity,
      };
    },

    //Remove cart
    [removeCartItem.fulfilled]: (state, { payload }) => {
      state.cartItems.splice(
        state.cartItems.findIndex((arrow) => arrow.id === +payload),
        1
      );

      state.status = "idle";
    },
    [removeCartItem.pending]: (state) => {
      state.status = "removing";
    },
    [removeCartItem.rejected]: (state) => {},

    //Remove cart with username
    [removeAllCartWithUserName.fulfilled]: (state, { payload }) => {
      state.cartItems = [];
      state.status = "idle";
    },
    [removeAllCartWithUserName.pending]: (state) => {
      state.status = "removing";
    },
    [removeAllCartWithUserName.rejected]: (state) => {},
  },
});

export default cartSlice;

export const { removeItem, resetCart } = cartSlice.actions;
