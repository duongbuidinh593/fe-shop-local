import style from "./CardImage.module.css";
import React from "react";

import { IconButton, Tooltip } from "@mui/material";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import LoopIcon from "@mui/icons-material/Loop";
import VisibilityIcon from "@mui/icons-material/Visibility";
import LocalMallIcon from "@mui/icons-material/LocalMall";
import { useSelector } from "react-redux/es/exports";
import { useDispatch } from "react-redux";
import { addToCartItem, checkItemExist } from "../../utils/HandleCartUtil";
import { toast } from "react-hot-toast";
import { useNavigate } from "react-router-dom";

const CardImage = ({ product, handleOpen }) => {
  const cart = useSelector((state) => state.cart).cartItems;
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handleAddToCart = () => {
    if (user.isLoggin === false) {
      navigate("/login");
      return;
    }
    const cartItem = {
      product_id: product.id,
      quantity: 1,
    };
    toast.success(`${product.name} was added`);
    const isExistCartItem = checkItemExist(cartItem, cart);
    addToCartItem(cartItem, isExistCartItem, dispatch);
  };

  return (
    <>
      <div className={style.cardContainer}>
        <a href="#" className={style.cardImageContainer}>
          <img src={product.image_urls[0]} alt="heelo world" />
        </a>

        <div className={style.listToolTip}>
          <li>
            <Tooltip title="Add to Cart" onClick={handleAddToCart}>
              <IconButton
                size="large"
                sx={{
                  borderRadius: "10px",
                  backgroundColor: "#000",
                  "&:hover": {
                    backgroundColor: "#000",
                    boxShadow: "0 4px 4px rgba(0,0,0,0.1)",
                    opacity: 0.7,
                  },
                }}
              >
                <LocalMallIcon
                  sx={{
                    color: "#fff",
                  }}
                />
              </IconButton>
            </Tooltip>
          </li>
          <li>
            <Tooltip title="Quick Shop">
              <IconButton
                size="large"
                onClick={handleOpen}
                sx={{
                  borderRadius: "10px",
                  backgroundColor: "#000",
                  "&:hover": {
                    backgroundColor: "#000",
                    boxShadow: "0 4px 4px rgba(0,0,0,0.1)",
                    opacity: 0.7,
                  },
                }}
              >
                <VisibilityIcon
                  sx={{
                    color: "#fff",
                  }}
                />
              </IconButton>
            </Tooltip>
          </li>
          <li>
            <Tooltip title="Compare">
              <IconButton
                size="large"
                sx={{
                  borderRadius: "10px",
                  backgroundColor: "#000",
                  "&:hover": {
                    backgroundColor: "#000",
                    boxShadow: "0 4px 4px rgba(0,0,0,0.1)",
                    opacity: 0.7,
                  },
                }}
              >
                <LoopIcon
                  sx={{
                    color: "#fff",
                  }}
                />
              </IconButton>
            </Tooltip>
          </li>
        </div>
        <Tooltip
          title="Add to Wishlist"
          sx={{
            position: "absolute",
            top: "32px",
            right: "32px",
            backgroundColor: "#fff",
            "&:hover": {
              backgroundColor: "white",
              boxShadow: "0 4px 4px rgba(0,0,0,0.1)",
            },
          }}
        >
          <IconButton>
            <FavoriteBorderIcon />
          </IconButton>
        </Tooltip>
      </div>
    </>
  );
};

export default CardImage;
