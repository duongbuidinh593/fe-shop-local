import React from "react";
import { useSelector } from "react-redux";
import ProductCard from "./ProductCard";
import { Container } from "@mui/material";
import { Swiper, SwiperSlide } from "swiper/react";
import style from "./TrendingSlider.module.css";
import axios from "axios";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import { Pagination, Navigation } from "swiper";

const TrendingSlider = () => {
  const productsTrending = useSelector((state) => state.productList.products);
  const slideProducts = productsTrending.slice(0, 6);
  return (
    <Container sx={{ marginY: "72px" }}>
      <div className={style.textContainer}>
        <h1 className={style.textPrimary}>Trending</h1>
      </div>

      <Swiper
        slidesPerView={3}
        spaceBetween={30}
        slidesPerGroup={3}
        loop={true}
        loopFillGroupWithBlank={true}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        modules={[Navigation]}
      >
        {slideProducts.map((product) => (
          <SwiperSlide>
            <ProductCard product={product} id={product.id} key={product.id} />
          </SwiperSlide>
        ))}
      </Swiper>
    </Container>
  );
};

export default TrendingSlider;
