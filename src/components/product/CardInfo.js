import React from "react";
import style from "./CardInfo.module.css";
import Price from "./Price";

const CardInfo = (props) => {
  return (
    <div className={style.CardInfoContainer}>
      <p className={style.CardName}>{props.name}</p>
      <Price price={props.price} discount={props.discount} />
    </div>
  );
};

export default CardInfo;
