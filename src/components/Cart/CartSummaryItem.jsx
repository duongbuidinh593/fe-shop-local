import React, { useState } from "react";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { useDispatch, useSelector } from "react-redux";
import {
  removeCartItem,
  updateCart,
} from "../../redux/features/cart/cartActions";
import { toast } from "react-hot-toast";

const CartSummaryItem = ({ cartItem }) => {
  const [quantity, setQuantity] = useState(cartItem.quantity);

  const dispatch = useDispatch();
  const handleReduceQuantity = () => {
    if (cartItem.quantity - 1 === 0) {
      toast.success(`${cartItem.name} was removed`);

      dispatch(removeCartItem(cartItem.id));
    } else {
      dispatch(
        updateCart({ cartID: cartItem.id, quantity: cartItem.quantity - 1 })
      );
      setQuantity(cartItem.quantity - 1);
    }
  };
  const handleIncreQuantity = () => {
    dispatch(
      updateCart({ cartID: cartItem.id, quantity: cartItem.quantity + 1 })
    );
    setQuantity(cartItem.quantity + 1);
  };
  const discountPrice =
    cartItem.price - Math.floor((cartItem.price * cartItem.discount) / 100);

  return (
    <div className="mt-4 md:mt-6 flex border  flex-col md:flex-row justify-start items-start md:items-center md:space-x-6 xl:space-x-8 w-full ">
      <div className="pb-4 md:pb-8 w-full md:w-40">
        <img
          className="w-full md:block"
          src={cartItem.image_urls[0]}
          alt="dress"
        />
      </div>
      <div className="md:flex-row  flex-col flex justify-between items-start w-full  pb-8 space-y-4 md:space-y-0">
        <div className="w-full flex flex-col justify-start items-start space-y-8">
          <h3 className="text-xl xl:text-2xl font-semibold leading-6 text-gray-800">
            {cartItem.name}
          </h3>
        </div>
        <div className="flex justify-between space-x-8 items-start w-full">
          <div className="w-24">
            {" "}
            {discountPrice === cartItem.price ? (
              <p className="text-base xl:text-lg leading-6">
                ${cartItem.price}
              </p>
            ) : (
              <p className="text-base xl:text-lg leading-6">
                ${discountPrice}
                <span className="text-gray-300 block line-through">
                  ${cartItem.price}
                </span>
              </p>
            )}
          </div>

          <div className="flex items-center gap-2 ">
            <button
              key={"button1"}
              className="text-[24px]"
              onClick={handleReduceQuantity}
            >
              <RemoveIcon />
            </button>
            <p className="text-base w-16 text-center inline-block px-4 py-2 xl:text-lg leading-6 text-gray-800 border rounded-sm">
              {quantity}
            </p>
            <button key="button2" onClick={handleIncreQuantity}>
              <AddIcon />
            </button>
          </div>
          <p
            className="text-base xl:text-lg font-semibold leading-6 text-gray-800"
            style={{ marginRight: "16px" }}
          >
            ${discountPrice * cartItem.quantity}
          </p>
        </div>
      </div>
    </div>
  );
};

export default CartSummaryItem;
