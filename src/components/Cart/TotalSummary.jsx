import { Button } from "@mui/material";
import React from "react";
import AccountBalanceWalletOutlinedIcon from "@mui/icons-material/AccountBalanceWalletOutlined";
import { Link } from "react-router-dom";

const TotalSummary = (props) => {
  let discountPercent = Math.floor((props.discount * 100) / props.subTotal);
  return (
    <div className="flex flex-col px-4 py-4 md:p-6 xl:p-8 w-full bg-gray-50 space-y-6   ">
      <h3 className="text-xl font-semibold leading-5 text-gray-800">Summary</h3>
      <div className="flex justify-center items-center w-full space-y-4 flex-col border-gray-200 border-b pb-4">
        <div className="flex justify-between  w-full">
          <p className="text-base leading-4 text-gray-800">Subtotal</p>
          <p className="text-base leading-4 text-gray-600">
            ${parseFloat(props.subTotal).toFixed(2)}
          </p>
        </div>
        <div className="flex justify-between items-center w-full">
          <p className="text-base leading-4 text-gray-800">
            Discount{" "}
            <span className="bg-gray-200 p-1 text-[16px] font-medium leading-3  text-gray-800">
              Sale
            </span>
          </p>
          <p className="text-base leading-4 text-gray-600">
            -${parseFloat(props.discount).toFixed(2)} (Sale : {discountPercent}
            %)
          </p>
        </div>
        <div className="flex justify-between items-center w-full">
          <p className="text-base leading-4 text-gray-800">Shipping</p>
          <p className="text-base leading-4 text-gray-600">$8.00</p>
        </div>
      </div>
      <div className="flex justify-between items-center w-full">
        <p className="text-base font-semibold leading-4 text-gray-800">Total</p>
        <p className="text-base font-semibold leading-4 text-gray-600">
          ${parseFloat(props.subTotal - props.discount + 8).toFixed(2)}
        </p>
      </div>

      <div className="border  py-4 text-[18px] text-white bg-slate-800 cursor-pointer">
        <Link className="block text-center" to={"/check_out"}>
          Check Out
        </Link>
      </div>
    </div>
  );
};

export default TotalSummary;
