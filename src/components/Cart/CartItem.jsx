import React from "react";

const CartItem = ({ cartItem }) => {
  return (
    <div className="flex items-center pt-2 pb-2 last:pb-6">
      <img
        className="object-cover   inline-block border w-20 h-20 rounded-lg"
        src={cartItem.image_urls[0]}
        alt="cart item image"
      />

      <div className="ml-4 text-gray-600">
        <h3 className="text-[18px] font-medium ">{cartItem.name}</h3>

        <dl className="mt-1 space-y-1 text-sm">
          <div>
            <dt className="inline">Quantity:</dt>
            <dd className="inline">{cartItem.quantity}</dd>
          </div>
          <div>
            <dt className="inline">Price:</dt>
            <dd className="inline">${cartItem.price}</dd>
          </div>
        </dl>
      </div>
    </div>
  );
};

export default CartItem;
