import React from "react";

const Shipping = () => {
  return (
    <div className="flex flex-col justify-center px-4 py-6 md:p-6 xl:p-8 w-full bg-gray-50 space-y-6   ">
      <h3 className="text-xl font-semibold leading-5 text-gray-800">
        Shipping
      </h3>
      <div className="flex justify-between items-start w-full">
        <div className="flex justify-center items-center space-x-4">
          <div class="w-8 h-8">
            <img
              class="w-full h-full"
              alt="logo"
              src="https://i.ibb.co/L8KSdNQ/image-3.png"
            />
          </div>
          <div className="flex flex-col justify-start items-center">
            <p className="text-lg leading-6 font-semibold text-gray-800">
              DPD Delivery
              <br />
              <span className="font-normal">Delivery with 24 Hours</span>
            </p>
          </div>
        </div>
        <p className="text-lg font-semibold leading-6 text-gray-800">$8.00</p>
      </div>
    </div>
  );
};

export default Shipping;
