import { Badge } from "@mui/material";
import React from "react";
import { MdOutlineLocalGroceryStore } from "react-icons/md";
import { Link } from "react-router-dom";
import style from "./Cart.module.css";
import CartItem from "./CartItem";
import { useSelector } from "react-redux";
import Loading from "../Loading/Loading";

const Cart = () => {
  const cart = useSelector((state) => state.cart);
  if (cart.status == "loading") {
    return <Loading />;
  }
  const cartItems = cart.cartItems;
  return (
    <span className={`relative ${style.cartContainer} `}>
      <Badge badgeContent={cartItems.length} color="primary">
        <MdOutlineLocalGroceryStore />
      </Badge>
      <div
        className={`${style.cartBox} absolute top-[100%]  rounded-md shadow-2xl right-[0] z-40 w-screen max-w-sm p-10  sm:px-12 bg-white border-stone-600 `}
        aria-modal="true"
        aria-label="Item added to your cart"
        role="dialog"
        tabindex="-1"
      >
        <div className="flex items-start justify-end ">
          <button
            className="-mt-6 -mr-6 transition-transform sm:-mr-8 hover:scale-110 "
            type="button"
            aria-label="Close"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-5 h-5"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          </button>
        </div>
        <div className="max-h-[200px] overflow-y-auto mb-6">
          {cartItems.length === 0 ? (
            <div className="w-[60%] border-t-2 border-t-gray-300 mx-auto my-0">
              <img
                className="object-cover"
                alt="empty cart"
                src="https://res.cloudinary.com/duqtjopvf/image/upload/v1659955379/shop/empty-cart_zkqopr.svg"
              />
              <span className="text-[18px] font-medium text-gray-600 text-center block mt-3">
                Cart is empty
              </span>
            </div>
          ) : (
            cartItems.map((cartItem) => <CartItem cartItem={cartItem} />)
          )}
        </div>

        <div className="space-y-4 text-center text-gray-600">
          <Link
            className="block p-3 text-sm border rounded-lg border-stone-600  hover:ring-1 hover:ring-stone-400 hover:text-stone-600"
            to="/order_summary"
          >
            View my cart ({cartItems.length})
          </Link>

          <Link
            to={"/check_out"}
            className="block w-full p-3 text-sm rounded-lg bg-stone-600 text-white hover:bg-stone-500"
            type="submit"
          >
            Check out
          </Link>

          <Link
            className="inline-block text-sm tracking-wide underline underline-offset-4  hover:text-stone-600"
            to={"/products"}
          >
            Continue shopping
          </Link>
        </div>
      </div>
    </span>
  );
};

export default Cart;
