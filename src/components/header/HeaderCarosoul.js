import React, { useRef, useState } from "react";

import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper";
import "swiper/css/autoplay";

// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-fade";
import "swiper/css/navigation";
import "swiper/css/pagination";

//import "./styles.css";
import style from "./HeaderCarosoul.module.css";
import "./HeaderCarosoulArrow.css";
// import required modules
import { EffectFade, Navigation, Pagination } from "swiper";

export default function App() {
  return (
    <>
      <Swiper
        spaceBetween={30}
        effect={"fade"}
        navigation={true}
        loop={true}
        // autoplay={{ delay: 3000 }}

        pagination={{
          clickable: true,
        }}
        modules={[EffectFade, Navigation, Pagination, Autoplay]}
        className={style.Swiper}
      >
        <SwiperSlide className={style.swiperSlide}>
          <div className={style.bg1}>
            <div className={style.textHeader1}>
              <div>
                <h1 className={style.textPrimary}>Furniture Life</h1>
                <p className={style.textSecondary}>
                  Learning is cool, but knowing is better, and I know the key to
                  success.
                </p>
              </div>
              <a href="#" className={style.primaryButton}>
                View more →
              </a>
            </div>
          </div>
        </SwiperSlide>

        <SwiperSlide className={style.swiperSlide}>
          <div className={style.bg2}>
            <div className={style.textHeader2}>
              <div>
                <h1 className={style.textPrimary}>Summer Sale</h1>
                <p className={style.textSecondary}>
                  Learning is cool, but knowing is better, and I know the key to
                  success
                </p>
              </div>

              <a href="#" className={style.primaryButton}>
                Shop now→
              </a>
            </div>
          </div>
        </SwiperSlide>
      </Swiper>
    </>
  );
}
