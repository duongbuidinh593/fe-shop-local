import React, { useState } from "react";
import Rating from "@mui/material/Rating";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { useFormik } from "formik";
import { addToCartItem, checkItemExist } from "./../../utils/HandleCartUtil";
import { toast } from "react-hot-toast";
import { useNavigate } from "react-router-dom";

const ProductDetailInfo = ({ product }) => {
  const cart = useSelector((state) => state.cart).cartItems;
  const navigate = useNavigate();
  const user = useSelector((state) => state.user);

  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      quantity: 1,
    },
    onSubmit: (values) => {
      if (user.isLoggin === false) {
        navigate("/login");
        return;
      }
      const cartItem = {
        product_id: product.id,
        quantity: values.quantity,
      };
      const isExistCartItem = checkItemExist(cartItem, cart);
      toast.success(`${product.name} was added`);
      addToCartItem(cartItem, isExistCartItem, dispatch);
    },
  });
  let listDescription = product.description.split("-");
  const [quantity, setQuantity] = useState(1);
  const handleChangeInput = (event) => {
    setQuantity(event.target.value);
  };

  return (
    <div className="sticky top-0">
      <div className="flex justify-between mt-8">
        <div className="max-w-[35ch]">
          <h1 className="text-2xl font-bold">{product.name}</h1>

          <p className="mt-0.5 text-sm">Highest Rated Product</p>

          <div className="flex mt-2 -ml-0.5">
            <Rating name="half-rating" defaultValue={2.5} precision={0.5} />
          </div>
        </div>

        <p className="text-lg font-bold">${product.price}</p>
      </div>

      <details className="relative mt-4 ">
        <summary className="block">
          <div>
            <div className="prose max-w-none">
              <ul>
                {listDescription.map((text, index) => (
                  <li>-{text}</li>
                ))}
              </ul>
            </div>
          </div>
        </summary>
      </details>

      <form className="mt-8" onSubmit={formik.handleSubmit}>
        <div className="flex mt-8">
          <div>
            <label for="quantity" className="sr-only">
              Qty
            </label>

            <input
              type="number"
              id="quantity"
              min="1"
              value={formik.values.quantity}
              onChange={formik.handleChange}
              className="shadow-sm bg-[#f4f4f4] py-5 px-4 inline-block text-xs text-center border-gray-200 rounded no-spinners"
            />
          </div>

          <button
            type="submit"
            className="block w-full px-5 py-4 ml-3 text-base font-medium text-white bg-green-600 rounded hover:bg-green-500"
          >
            Add to Cart
          </button>
        </div>
        <button
          type="submit"
          className="block w-full py-4 mt-3  font-medium text-base hover:bg-slate-800 text-white bg-black rounded "
        >
          Buy it now
        </button>
      </form>
    </div>
  );
};

export default ProductDetailInfo;
