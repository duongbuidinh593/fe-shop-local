import React from "react";
import CollectionProducts from "../ProductsCollection/CollectionProducts";
import FilterTab from "../ProductsCollection/FilterTab";
import { useSelector } from "react-redux";
import Loading from "../Loading/Loading";
import { useState } from "react";
import { useEffect } from "react";

const ProductCollectionsPage = () => {
  const productList = useSelector((state) => state.productList);

  const [products, setProducts] = useState([]);

  useEffect(() => {
    setProducts(productList.products);
  }, [productList.status]);

  if (productList.status === "loading") {
    return <Loading />;
  }

  return (
    <section>
      <div className="max-w-screen-xl px-4 py-12 mx-auto sm:px-6 lg:px-8">
        <div className="grid grid-cols-1 gap-4 lg:grid-cols-4 lg:items-start">
          <FilterTab
            setProducts={setProducts}
            allProducts={productList.products}
          />

          <CollectionProducts products={products} setProducts={setProducts} />
        </div>
      </div>
    </section>
  );
};

export default ProductCollectionsPage;
