import { Container, Grid, Item } from "@mui/material";
import React from "react";
import style from "./FeaturedProductTable.module.css";
import { useSelector } from "react-redux";
import ProductCard from "../product/ProductCard";

const FeaturedProductTable = (props) => {
  const listFeatuteredProducts = props.featuredProducts;
  return (
    <Container>
      <Grid container spacing={2} key="grid1">
        {listFeatuteredProducts.map((product) => (
          <Grid item lg={3} key={`grid ${product.id}`}>
            <ProductCard product={product} key={product.id} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default FeaturedProductTable;
