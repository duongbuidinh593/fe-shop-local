import * as React from "react";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import FeaturedProductTable from "./FeaturedProductTable";
import { Container } from "@mui/material";
import style from "./FeaturedProductTab.module.css";
import { useSelector } from "react-redux";

export default function LabTabs() {
  const [value, setValue] = React.useState("1");

  const products = useSelector((state) => state.productList.products);
  const tabFeaturedProduct1 = products
    .filter((product) => product.type === "Jewelery")
    .slice(0, 8);
  const tabFeaturedProduct2 = products
    .filter((product) => product.type === "Men's Clothing")
    .slice(0, 8);
  const tabFeaturedProduct3 = products
    .filter((product) => product.type === "Electronics")
    .slice(0, 8);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Container key="featuredProductTabctn">
      <div className={style.textContainer}>
        <h1 className={style.textPrimary}>Featured Products</h1>
      </div>
      <TabContext value={value}>
        <Container sx={{ margin: "0 auto" }}>
          <TabList
            onChange={handleChange}
            aria-label="lab API tabs example"
            centered
          >
            <Tab
              key="tab1"
              label="Jewelery"
              sx={{ textTransform: "initial" }}
              value="1"
            />
            <Tab
              key="tab2"
              label="Men's Clothing"
              sx={{ textTransform: "initial" }}
              value="2"
            />
            <Tab
              key="tab3"
              label="Electronics"
              sx={{ textTransform: "initial" }}
              value="3"
            />
          </TabList>
        </Container>
        <TabPanel value="1" key="tabpanel1">
          <FeaturedProductTable
            key="fp1"
            featuredProducts={tabFeaturedProduct1}
          />
        </TabPanel>
        <TabPanel value="2" key="tabpanel2">
          <FeaturedProductTable
            key="fp2"
            featuredProducts={tabFeaturedProduct2}
          />
        </TabPanel>
        <TabPanel value="3" key="tp3">
          <FeaturedProductTable
            key="fp3"
            featuredProducts={tabFeaturedProduct3}
          />
        </TabPanel>
      </TabContext>
    </Container>
  );
}
