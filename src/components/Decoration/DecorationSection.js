import style from "./DecorationSection.module.css";

import React from "react";
import { Container } from "@mui/material";

const DecorationSection = () => {
  return (
    <Container>
      <div className={style.DecorationSectionContainer}>
        <div className={style.imgContainer}>
          <img
            src="https://cdn.shopify.com/s/files/1/0279/3317/9952/files/book-1_1500x_1920x_f67e497b-5b84-416e-bfdf-c488906e4746_1920x.jpg?v=1572849456"
            alt="anh"
          />
        </div>
        <div className={style.imgContainer}>
          <img
            src="https://cdn.shopify.com/s/files/1/0279/3317/9952/files/book-3_1500x_1920x_f34c8d9a-8d0e-4192-8412-f4dc15014a73_1920x.jpg?v=1572849456"
            alt="anh"
          />
        </div>
        <div className={style.imgContainer}>
          <img
            src="https://cdn.shopify.com/s/files/1/0279/3317/9952/files/book-2_1500x_1920x_a73fcfc8-9c96-4eac-bcba-def38cf3eb4a_1920x.jpg?v=1572849456"
            alt="anh"
          />
        </div>
      </div>
    </Container>
  );
};

export default DecorationSection;
