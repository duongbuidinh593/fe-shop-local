import React from "react";
import * as yup from "yup";
import { useFormik } from "formik";
import { Alert, TextField } from "@mui/material";
import { useDispatch } from "react-redux";
import { createOrder } from "../../redux/features/order/orderActions";
import { removeAllCartWithUserName } from "./../../redux/features/cart/cartActions";
import { getAllOrder } from "./../../redux/features/orderAdmin/orderAdminActions";
import { fetchAllOrder } from "./../../redux/features/order/orderActions";
const schema = yup
  .object({
    FirstName: yup.string().required(),
    LastName: yup.string().required(),
    Address: yup.string().required(),
    PhoneNumber: yup.string().required(),
    Comments: yup.string().notRequired(),
  })
  .required();
const CheckOutForm = ({ cartItems }) => {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      FirstName: "",
      LastName: "",
      Address: "",
      PhoneNumber: "",
      Comments: "",
    },
    validationSchema: schema,
    onSubmit: (values, actions) => {
      dispatch(
        createOrder({
          customer_name: values.FirstName + " " + values.LastName,
          order_address: values.Address,
          cartItems,
          comments: values.Comments,
        })
      );
      dispatch(removeAllCartWithUserName());
      dispatch(getAllOrder());
      dispatch(fetchAllOrder());
      actions.resetForm();
    },
  });

  return (
    <div className="flex mb-12 w-full sm:w-9/12 lg:w-full flex-col lg:flex-row justify-center items-center lg:space-x-10 2xl:space-x-36 space-y-12 lg:space-y-0">
      <div className="flex w-full  flex-col justify-start items-start">
        <div>
          <p className="text-3xl lg:text-4xl text-center font-semibold leading-7 lg:leading-9 text-gray-800">
            Order
          </p>
        </div>

        <div className="mt-2">
          <p className="text-xl font-semibold leading-5 text-gray-800">
            Shipping Details
          </p>
        </div>
        <form
          onSubmit={formik.handleSubmit}
          className="mt-8 flex flex-col justify-start items-start w-full space-y-8 "
        >
          {formik.errors.FirstName && formik.touched.FirstName && (
            <Alert sx={{}} severity="error" key="first_name_alert">
              {formik.errors.FirstName}
            </Alert>
          )}
          <TextField
            fullWidth
            key={"firstname"}
            type="text"
            label="First name"
            name="FirstName"
            value={formik.values.FirstName}
            onChange={formik.handleChange}
            sx={{ marginBottom: "12px" }}
          />

          {formik.errors.LastName && formik.touched.LastName && (
            <Alert
              sx={{ marginBottom: "2px" }}
              severity="error"
              key="last_name_alert"
            >
              {formik.errors.LastName}
            </Alert>
          )}
          <TextField
            fullWidth
            key={"lastname"}
            type="text"
            label="Last name"
            name="LastName"
            value={formik.values.LastName}
            onChange={formik.handleChange}
            sx={{ marginBottom: "12px" }}
          />

          {formik.errors.Address && formik.touched.Address && (
            <Alert
              sx={{ marginBottom: "2px" }}
              severity="error"
              key="address_alert"
            >
              {formik.errors.Address}
            </Alert>
          )}
          <TextField
            fullWidth
            key={"Address"}
            type="text"
            label="Address"
            name="Address"
            value={formik.values.Address}
            onChange={formik.handleChange}
            sx={{ marginBottom: "12px" }}
          />

          {formik.errors.PhoneNumber && formik.touched.PhoneNumber && (
            <Alert
              sx={{ marginBottom: "2px" }}
              severity="error"
              key="phone_alert"
            >
              {formik.errors.PhoneNumber}
            </Alert>
          )}
          <TextField
            fullWidth
            key={"phonenumber"}
            type="text"
            label="Phone Number"
            name="PhoneNumber"
            value={formik.values.PhoneNumber}
            onChange={formik.handleChange}
            sx={{ marginBottom: "12px" }}
          />

          {formik.errors.Comments && formik.touched.Comments && (
            <Alert
              sx={{ marginBottom: "2px" }}
              severity="error"
              key="comment_alert"
            >
              {formik.errors.Comments}
            </Alert>
          )}
          <TextField
            fullWidth
            key={"Comment"}
            type="text"
            label="Comment"
            name="Comments"
            value={formik.values.Comments}
            onChange={formik.handleChange}
            sx={{ marginBottom: "12px" }}
          />
          <button
            type="submit"
            className="focus:outline-none focus:ring-2 focus:ring-gray-500 focus:ring-offset-2 mt-8 text-base font-medium focus:ring-2 focus:ring-ocus:ring-gray-800 leading-4 hover:bg-black py-4 w-full md:w-4/12 lg:w-full text-white bg-gray-800"
          >
            Proceed to payment
          </button>
        </form>

        <div className="mt-4 flex justify-start items-center w-full">
          <a
            href="javascript:void(0)"
            className="text-base leading-4  underline focus:outline-none focus:text-gray-500  hover:text-gray-800 text-gray-600"
          >
            Back to my bag
          </a>
        </div>
      </div>
    </div>
  );
};

export default CheckOutForm;
