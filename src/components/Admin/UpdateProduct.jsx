import React, { useEffect, useState } from "react";

import { Container } from "@mui/system";
import * as yup from "yup";
import { useFormik } from "formik";
import { Alert, IconButton, styled, TextField } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import axios from "axios";
import { multipartFormHeadersContentType } from "../../utils/Config";
import { useDispatch, useSelector } from "react-redux";
import {
  createProduct,
  updateProduct,
} from "./../../redux/features/products/productActions";
import { LoadingButton as _LoadingButton } from "@mui/lab";
import {
  resetSuccess,
  resetUpdateSuccess,
} from "../../redux/features/products/productListSlice";
import { toast } from "react-hot-toast";
import { useParams } from "react-router-dom";

const schema = yup.object({
  Name: yup.string().required(),
  Price: yup.number().required().min(1),
  Type: yup.string().required(),
  Description: yup.string().notRequired(),
  Discount: yup.number().default(0),
  ImageBytes: yup.array().required(),
});

const LoadingButton = styled(_LoadingButton)`
  padding: 0.6rem 0;
  background-color: #000;
  color: #f2f2f2;
  font-weight: 500;

  &:hover {
    background-color: rgba(0, 0, 0, 0.7);
    transform: translateY(-2px);
  }
`;

const UpdateProductPage = () => {
  const dispatch = useDispatch();
  const { id } = useParams();

  const { isLoading, updatesuccess, error } = useSelector(
    (state) => state.productList
  );

  const products = useSelector((state) => state.productList).products;
  const product = products.find((p) => p.id === +id);

  const [imageFiles, setImageFiles] = useState([]);
  const [images, setImages] = useState([]);

  useEffect(() => {
    if (updatesuccess) {
      toast.success("Product was edited!");
      dispatch(resetUpdateSuccess());
    }

    if (error) {
      toast.error(error);
    }
  }, [updatesuccess]);

  useEffect(() => {
    const fileReaders = [];
    let isCancel = false;
    if (imageFiles.length) {
      const promises = imageFiles.map((file) => {
        return new Promise((resolve, reject) => {
          const fileReader = new FileReader();
          fileReaders.push(fileReader);
          fileReader.onload = (e) => {
            const { result } = e.target;
            if (result) {
              resolve(result);
            }
          };
          fileReader.onabort = () => {
            reject(new Error("File reading aborted"));
          };
          fileReader.onerror = () => {
            reject(new Error("Failed to read file"));
          };
          fileReader.readAsDataURL(file);
        });
      });
      Promise.all(promises)
        .then((images) => {
          if (!isCancel) {
            setImages(images);
          }
        })
        .catch((reason) => {
          console.log(reason);
        });
    }
    return () => {
      isCancel = true;
      fileReaders.forEach((fileReader) => {
        if (fileReader.readyState === 1) {
          fileReader.abort();
        }
      });
    };
  }, [imageFiles]);

  const formik = useFormik({
    initialValues: {
      Name: product.name,
      Price: product.price,
      Type: product.type,
      Description: product.description,
      Discount: product.discount,
      ImageBytes: [],
      ImageUrls: product.image_urls,
    },
    validationSchema: schema,
    onSubmit: (values, actions) => {
      setImageFiles([]);
      setImages([]);
      dispatch(
        updateProduct({
          values,
          imageFiles,
          id,
          public_id_images: product.public_id_images,
        })
      );
    },
  });

  return (
    <Container>
      <div>
        <h1 className="text-4xl mt-12 mb-8 text-center ">Edit Product</h1>
        <form
          className="w-2/3 mx-auto my-0 shadow-lg px-12 py-12"
          onSubmit={formik.handleSubmit}
        >
          {formik.errors.Name && formik.touched.Name && (
            <Alert
              sx={{ marginBottom: "16px" }}
              severity="error"
              key="name_product"
            >
              {formik.errors.Name}
            </Alert>
          )}

          <TextField
            fullWidth
            label="Name"
            name="Name"
            key="Name"
            value={formik.values.Name}
            onChange={formik.handleChange}
            sx={{
              marginBottom: "12px",
            }}
          />

          {formik.errors.Price && formik.touched.Price && (
            <Alert
              sx={{ marginBottom: "16px" }}
              severity="error"
              key="price_product"
            >
              {formik.errors.Price}
            </Alert>
          )}
          <TextField
            fullWidth
            label="Price"
            name="Price"
            key="Price"
            value={formik.values.Price}
            onChange={formik.handleChange}
            sx={{
              marginBottom: "12px",
            }}
          />

          {formik.errors.Type && formik.touched.Type && (
            <Alert
              sx={{ marginBottom: "16px" }}
              severity="error"
              key="type_product"
            >
              {formik.errors.Type}
            </Alert>
          )}
          <TextField
            fullWidth
            label="Type"
            name="Type"
            key="Type"
            value={formik.values.Type}
            onChange={formik.handleChange}
            sx={{
              marginBottom: "12px",
            }}
          />

          {formik.errors.Description && formik.touched.Description && (
            <Alert
              sx={{ marginBottom: "16px" }}
              severity="error"
              key="description_product"
            >
              {formik.errors.Description}
            </Alert>
          )}
          <TextField
            fullWidth
            label="Description"
            name="Description"
            key="Description"
            value={formik.values.Description}
            onChange={formik.handleChange}
            sx={{
              marginBottom: "12px",
            }}
          />

          {formik.errors.Discount && formik.touched.Discount && (
            <Alert
              sx={{ marginBottom: "16px" }}
              severity="error"
              key="discount_product"
            >
              {formik.errors.Discount}
            </Alert>
          )}
          <TextField
            fullWidth
            label="Discount"
            name="Discount"
            key="Discount"
            value={formik.values.Discount}
            onChange={formik.handleChange}
            sx={{
              marginBottom: "12px",
            }}
          />

          <div className="flex justify-center items-center w-full  mx-auto my-0">
            <label
              for="dropzone-file"
              className="flex flex-col justify-center items-center w-full h-48 bg-gray-50 rounded-lg border-2 border-gray-300  cursor-pointer dark:hover:bg-bray-800 dark:bg-gray-200 hover:bg-gray-500 dark:border-gray-300 dark:hover:border-gray-300 dark:hover:bg-gray-300"
            >
              <input
                id="dropzone-file"
                type="file"
                className="hidden"
                name="ImageBytes"
                onChange={(event) => {
                  const files = event.target.files;
                  let myFiles = Array.from(files);
                  setImageFiles(myFiles);
                  formik.setFieldValue("ImageBytes", myFiles);
                }}
                multiple
              />
              {images.length > 0 ? (
                <div className="flex flex-shrink gap-5">
                  {images.map((image, idx) => {
                    return (
                      <div key={idx} id={idx} className="relative ">
                        <div>
                          {" "}
                          <img src={image} className="rounded-md h-36" alt="" />
                        </div>
                        <div className="absolute top-[-16px] right-[-16px] bg-slate-300 shadow-lg rounded-full hover:bg-slate-500">
                          <IconButton
                            onClick={() => {
                              setImageFiles(
                                (prev) =>
                                  (prev = imageFiles.filter(
                                    (item, id) => id !== idx
                                  ))
                              );
                              setImages(
                                (prev) =>
                                  (prev = images.filter(
                                    (item, id) => id !== idx
                                  ))
                              );
                            }}
                            aria-label="delete"
                            key={idx}
                            image_id={idx}
                            sx={{ color: "#fff9f3" }}
                          >
                            <DeleteIcon />
                          </IconButton>
                        </div>
                      </div>
                    );
                  })}
                </div>
              ) : (
                <div className="flex flex-col justify-center items-center pt-5 pb-6">
                  <svg
                    aria-hidden="true"
                    className="mb-3 w-10 h-10 text-gray-400"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"
                    ></path>
                  </svg>
                  <p className="mb-2 text-sm text-gray-500 dark:text-gray-400">
                    <span className="font-semibold">Click to upload </span> or
                    drag and drop a update image!
                  </p>
                  <p className="text-xs text-gray-500 dark:text-gray-400">
                    SVG, PNG, JPG
                  </p>
                </div>
              )}
            </label>
          </div>
          <LoadingButton
            variant="contained"
            sx={{ mt: 1 }}
            fullWidth
            disableElevation
            type="submit"
            loading={isLoading}
          >
            Update
          </LoadingButton>
        </form>
      </div>
    </Container>
  );
};

export default UpdateProductPage;
