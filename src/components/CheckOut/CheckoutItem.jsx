import React from "react";

const CheckoutItem = ({ item }) => {
  const discountPrice =
    item.price - Math.floor((item.price * item.discount) / 100);

  return (
    <li className="flex items-center justify-between py-4 mr-6">
      <div className="flex items-start">
        <img
          className="flex-shrink-0 object-cover w-16 h-16 rounded-lg"
          src={item.image_urls[0]}
          alt=""
        />

        <div className="ml-4 ">
          <p className="text-sm my-5">{item.name}</p>
        </div>
      </div>

      <div>
        {discountPrice === item.price ? (
          <p className="text-sm">
            ${parseFloat(item.price).toFixed(2)}
            <small className="text-gray-500">x{item.quantity}</small>
          </p>
        ) : (
          <p className="text-sm">
            <span className="text-gray-400 line-through">
              ${parseFloat(item.price).toFixed(2)}
            </span>
            {` $${parseFloat(discountPrice).toFixed(2)}`}
            <small className="text-gray-500">x{item.quantity}</small>
          </p>
        )}
      </div>
    </li>
  );
};

export default CheckoutItem;
