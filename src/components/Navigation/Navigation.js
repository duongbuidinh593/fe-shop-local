import { Container } from "@mui/material";
import React, { useEffect } from "react";
import { AiOutlineUser, AiOutlineLock, AiOutlineHeart } from "react-icons/ai";
import { IoIosGitCompare } from "react-icons/io";
import { Link, useNavigate } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";

import { useSelector } from "react-redux/es/exports";
import LogoutOutlinedIcon from "@mui/icons-material/LogoutOutlined";
import style from "./Navigation.module.css";
import { useDispatch } from "react-redux";
import { autoLoggin, logOut } from "../../redux/features/auth/userSlice";

import { resetCart } from "../../redux/features/cart/cartSlice";
import { resetOrder } from "../../redux/features/order/orderSlice";
import { loginUserFn, logoutUserFn } from "./../../redux/api/authAPI1";


const Navigation = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isLoggin, access_token, userInfo } = useSelector(
    (state) => state.user
  );
  const role = userInfo?.user?.role || "user";

  useEffect(() => {
    dispatch(autoLoggin());
  }, access_token);

  const handleLogout = () => {
    dispatch(resetCart());
    dispatch(resetOrder());
    dispatch(logOut());
    logoutUserFn();
    navigate("/");
  };

  const handleLogIn = () => {};
  return (
    <div className={style.navTop}>
      <Container
        sx={{ display: "flex", justifyContent: "flex-end", gap: "12px" }}
      >
        {!isLoggin && (
          <div>
            <ul className={style.navHeader}>
              <li key={uuidv4()}>
                <Link
                  to="/login"
                  className={style.navHeaderItem}
                  key={uuidv4()}
                >
                  <AiOutlineUser key={uuidv4()} />
                  <span key={uuidv4()}>Sign In</span>
                </Link>
              </li>
              <li key={uuidv4()}>
                <Link
                  to="register"
                  key={uuidv4()}
                  className={style.navHeaderItem}
                >
                  <AiOutlineLock key={uuidv4()} />
                  <span key={uuidv4()}>Create Account</span>
                </Link>
              </li>
            </ul>
          </div>
        )}
        {isLoggin && (
          <div>
            <ul className={style.navHeader}>
              <li key={uuidv4()}>
                <Link
                  to="/my_account"
                  className={style.navHeaderItem}
                  key={uuidv4()}
                >
                  <AiOutlineUser key={uuidv4()} />
                  <span key={uuidv4()}>My Account</span>
                </Link>
              </li>
              {role === "admin" ? (
                <li key={uuidv4()}>
                  <Link
                    to="/admin"
                    className={style.navHeaderItem}
                    key={uuidv4()}
                  >
                    <AiOutlineUser key={uuidv4()} />
                    <span key={uuidv4()}>Admin</span>
                  </Link>
                </li>
              ) : (
                ""
              )}
              <li key={uuidv4()}>
                <Link
                  to="/"
                  key={uuidv4()}
                  onClick={handleLogout}
                  className={style.navHeaderItem}
                >
                  <LogoutOutlinedIcon
                    sx={{ fontSize: "18px" }}
                    key={uuidv4()}
                  />
                  <span key={uuidv4()}>Log Out</span>
                </Link>
              </li>
            </ul>
          </div>
        )}
      </Container>
    </div>
  );
};

export default Navigation;
